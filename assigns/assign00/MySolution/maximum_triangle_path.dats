#include "share/atspre_staload.hats" 
#include
"libats/libc/DATS/math.dats"
#include
"share/atspre_define.hats" // defines some names
#include
"share/atspre_staload.hats" // for targeting C
#include
"share/HATS/atspre_staload_libats_ML.hats" // for ...
#include
"share/HATS/atslib_staload_libats_libc.hats" // for libc



(* ****** ****** *)

fun print_list(list: list0(int)): void =
  case+ list of
  | nil0() => println!(" ")
  | cons0(car, crd) =>
    let
      val () = begin print car; print ','; end
      val () = print_list(crd)
    in
    end

fun get_list_length(list: list0(int), length: int): int =
  case+ list of
  | nil0() => length
  | cons0(car, crd) => get_list_length(crd, length+1)

fun get_x_element(list: list0(int), tar: int): int = 
  case+ list of
  | nil0() => 0
  | cons0(car, crd) => if tar=0 then car else get_x_element(crd, tar-1)



fun modify_x_element(list: list0(int), tar: int, add: int): list0(int) = 
  case+ list of
  | nil0() => nil0()
  | cons0(car, crd) => if tar=0 then cons0(car+add, modify_x_element(crd, tar-1, add)) else cons0(car, modify_x_element(crd, tar-1, add))

fun d2i(x: double): int = $UN.cast{int}(x)


fun max_x_y(x: int, y: int): int = if x>y then x else y



fun max_t_path(step: int, stepc: int, tri: list0(int), i: int): void = 
if 0>i then println!("the maximum path is ", get_x_element(tri, 0))
else if ((stepc+1) = step) then max_t_path(step-1, 0, modify_x_element(tri, i, max(get_x_element(tri, step+i), get_x_element(tri, step+i+1))), i-1)
else max_t_path(step, stepc+1, modify_x_element(tri, i, max(get_x_element(tri, step+i), get_x_element(tri, step+i+1))), i-1)

implement
main0()=
{
val ys: list0(int) = (list0)$arrpsz{int}(55,94,48,95,30,96,77,71,26,67)
val zs: list0(int) =  (list0)$arrpsz{int}(55,94, 48,95, 30, 96,77, 71, 26, 67,97, 13, 76, 38, 45,7, 36, 79, 16, 37, 68,48, 7, 9, 18, 70, 26, 6,18, 72, 79, 46, 59, 79, 29, 90,20, 76, 87, 11, 32, 7, 7, 49, 18,27, 83, 58, 35, 71, 11, 25, 57, 29, 85,14, 64, 36, 96, 27, 11, 58, 56, 92, 18, 55,2, 90, 3, 60, 48, 49, 41, 46, 33, 36, 47, 23,92, 50, 48, 2, 36, 59, 42, 79, 72, 20, 82, 77, 42,56, 78, 38, 80, 39, 75, 2, 71, 66, 66, 1, 3, 55, 72,44, 25, 67, 84, 71, 67, 11, 61, 40, 57, 58, 89, 40, 56, 36,85, 32, 25, 85, 57, 48, 84, 35, 47, 62, 17, 1, 1, 99, 89, 52,6, 71, 28, 75, 94, 48, 37, 10, 23, 51, 6, 48, 53, 18, 74, 98, 15,27, 2, 92, 23, 8, 71, 76, 84, 15, 52, 92, 63, 81, 10, 44, 10, 69, 93)

val len: int = get_list_length(ys, 0)
val base: int = d2i((sqrt(8.0*len+1)-1)/2)
val() = max_t_path(base-1, 0, ys, len-base-1)

val len: int = get_list_length(zs, 0)
val base: int = d2i((sqrt(8.0*len+1)-1)/2)
val() = max_t_path(base-1, 0, zs, len-base-1)


}
