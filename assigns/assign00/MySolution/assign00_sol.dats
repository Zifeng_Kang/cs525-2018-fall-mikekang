(* ****** ****** *)
//
// How to test
// ./assign00_sol_dats
//
// How to compile:
// myatscc assign00_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign00.dats"

(* ****** ****** *)

implement
factorial(n) =
if n > 0
then n * factorial(n-1) else 1

implement
int_test() =
(fix
 loop(n: int, i: int): int =>
 if n > 0 then loop(n+n, i+1) else i
)(1, 1)


implement
gheep(n) = let
fun
loop(i: int, r0: int, r1: int): int =
  if i < (n-1) then loop(i+1, r1, (i+2)*r0*r1) else r1
in
  if n >= 1 then loop(0, 1, 2) else 1
end

local
fun
revapp
(
  xs: intlist, ys: intlist
) : intlist =
(
case+ xs of
| nil() => ys
| cons(x, xs) => revapp(xs, cons(x, ys))
)
in
implement
intlist_append
  (xs, ys) = revapp(revapp(xs, nil()), ys)
end

implement
main0() =
{
val () =
println!
("factorial(10) = ", factorial(10))

val () =
println! ("int_test() = ", int_test())

val () = println!(ghaap(1) = gheep(1))
val () = println!(ghaap(2) = gheep(2))
val () = println!(ghaap(5) = gheep(5))
val () = println!(ghaap(8) = gheep(8))

val xs = cons(0, cons(2, cons(4, nil())))
val ys = cons(1, cons(3, cons(5, nil())))
val () = fprintln! (stdout_ref, "xsys = ", $UNSAFE.cast{list0(int)}(intlist_append(xs, ys)))
} (* end of [main0] *)
(* ****** ****** *)
(* end of [assign00_sol.dats] *)
