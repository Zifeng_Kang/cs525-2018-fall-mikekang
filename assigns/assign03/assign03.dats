(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 525
//
// Semester: Fall, 2018
//
// Classroom: FLR 112
// Class Time: TR 2:00-3:15
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// HX: 50 points in total
//
// Due date: Tuesday, the 16th of October
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

typedef tvar = string
typedef topr = string

(* ****** ****** *)
//
datatype type =
  | TPbas of string
  | TPfun of (type, type)
  | TPtupl of list0(type)
//
(* ****** ****** *)

datatype
ctype = CTP of (list0(type), type)

(* ****** ****** *)
//
datatype term =
//
  | TMint of int // value
  | TMbool of bool // value
  | TMstring of string // value
//
  | TMvar of tvar // not evaluated
  | TMapp of (term, term) // non-value
  | TMlam of (tvar, type, term) // value
//
  | TMlet of (tvar, term, term) // let x = t1 in t2
  | TMopr of (topr, list0(term)) // primitive operators
//
  | TMift of (term, term, term)
  | TMfix of (tvar(*f*), tvar(*x*), type, type, term) // fixed-point opr
//
  | TMtupl of list0(term)
  | TMproj of (term, int(*index*)) // index starts from 0
//
(* ****** ****** *)

datatype value =
//
  | VALint of int
  | VALbool of bool
  | VALstring of string
//
  | VALclo of (term, envir)
  | VALfix of (term, envir)
//
  | VALtupl of list0(value)
//  
where
envir = list0($tup(tvar, value))

(* ****** ****** *)
//
extern
fun
print_type: print_type(type)
and
fprint_type: fprint_type(type)
//
overload print with print_type
overload fprint with fprint_type
//
(* ****** ****** *)
//
extern
fun
print_term: print_type(term)
and
fprint_term: fprint_type(term)
//
overload print with print_term
overload fprint with fprint_term
//
(* ****** ****** *)
//
extern
fun
print_value: print_type(value)
and
fprint_value: fprint_type(value)
//
overload print with print_value
overload fprint with fprint_value
//
(* ****** ****** *)
//
extern
val
theSig:
list0($tup(topr, ctype))
//
(* ****** ****** *)
//
// An implementation of the 8-queen puzzle is given below:
// http://ats-lang.sourceforge.net/DOCUMENT/INT2PROGINATS/HTML/HTMLTOC/x631.html
//
// Please implement a function of the following type such that
// interp0(TMqueenpuzzle(N)) prints out all the solutions to the N-queen puzzle
// In case N = 8, there are 92 solutions.
//
extern
fun
QueenPuzzle(N:int): term
//
(* ****** ****** *)

extern
fun tcheck0: (term) -> type
extern
fun interp0: (term) -> value

(* ****** ****** *)

(* end of [assign03.dats] *)
