(* ****** ****** *)
//
// How to test
// ./assign01_sol_dats
//
// How to compile:
// myatscc assign01_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign01.dats"

(* ****** ****** *)
//
// Please implement your code as follows:
//
(* ****** ****** *)


implement
tvar_count(t0: term, x: string): int=
(
case+ t0 of
| TMint(i) => 0
| TMvar(y) => if x=y then 1 else 0
| TMlam(y, t1) => if x=y then 0 else tvar_count(t1, x)
| TMapp(t1, t2) => tvar_count(t1, x)+tvar_count(t2, x)
)

extern
fun
term_in_lst(t0: tvar, tvarlst: list0(tvar)): bool

implement
term_in_lst(t0: tvar, tvarlst: list0(tvar)): bool = 
(
case+ tvarlst of
| nil0() => false
| cons0(car, cdr) => if t0=car then true else term_in_lst(t0, cdr)
)


extern
fun
close_helper(t0: term, tvarlst: list0(tvar)): bool

implement
close_helper(t0: term, tvarlst: list0(tvar)): bool = 
(
case+ t0 of
| TMint(i) => true
| TMvar(y) => if term_in_lst(y, tvarlst) then true else false
| TMlam(y, t1) => close_helper(t1, cons0(y, tvarlst))
| TMapp(t1, t2) => close_helper(t1, tvarlst) && close_helper(t2, tvarlst)
)


implement
term_is_closed(t0: term): bool =
close_helper(t0, nil0())

implement
main0() = {
val K = TMlam("x", TMlam("y", TMvar("x")))
val() = println!("K = ", K)
val count = tvar_count(K, "x")
val() = println!(count)
val S= TMapp(TMlam("x", TMvar("x")), TMlam("y", TMapp(TMvar("y"), TMvar("x"))))
val count = tvar_count(S, "x")
val() = println!(count)


val x = TMvar"x"
val y = TMvar"y"
val z = TMvar"z"

val omega = TMlam("x", x)
val Omega = TMapp(omega, omega)

val K = TMlam("x", TMlam("y", x))
val K' = TMlam("x", TMlam("y", y))

val () = assertloc(term_is_closed(K))
val () = assertloc(term_is_closed(K'))
val () = assertloc(term_is_closed(omega))
val () = assertloc(term_is_closed(Omega))

val () = assertloc(~term_is_closed(x))
val () = assertloc(~term_is_closed(TMapp(x, y)))

val() = println!("pass")

}

(* end of [assign01_sol.dats] *)
