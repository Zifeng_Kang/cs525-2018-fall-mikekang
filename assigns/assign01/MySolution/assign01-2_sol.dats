(* ****** ****** *)
//
// How to test
// ./assign01-2_sol_dats
//
// How to compile:
// myatscc assign01-2_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign01-2.dats"

(* ****** ****** *)
//
// Please implement your code as follows:
//
(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(t0) = 
fprint_term(stdout_ref, t0)

local

implement
fprint_val<term> = fprint_term

in (* in-of-local *)

implement
fprint_term
  (out, t0) =
(
case+ t0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMbool(x) =>
  fprint!(out, "TMbool(", x, ")")
| TMstring(x) =>
  fprint!(out, "TMstring(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "; ", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMlet(x, t1, t2) =>
  fprint!(out, "TMlet(", x, "; ", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts)
)

end

extern
fun
opr_subst_helper(ts: termlst, x: tvar, t: term): termlst


implement
opr_subst_helper(ts: termlst, x: tvar, t: term): termlst = 
(
case+ ts of
| nil0() => nil0()
| cons0(car, cdr) => cons0(subst(car, x, t), opr_subst_helper(cdr, x, t))
)

implement
subst(t0: term, x: tvar, t: term) =
(
case+ t0 of

| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
| TMvar y => if x = y then t else t0


| TMlam(y, t1) => if x = y then t0 else TMlam(y, subst(t1, x, t))

| TMapp(t1, t2) =>
  TMapp(subst(t1, x, t), subst(t2, x, t))

| TMlet
  (y, t1, t2) => TMlet(y, t, subst(t2, x, t))

| TMopr(opr, ts) => TMopr(opr, opr_subst_helper(ts, x, t))

)

implement
main0() = {
val() = println!("x")
val x = TMvar"x"
val z = TMvar"z"
val S= TMapp(TMlam("x", TMvar("x")), TMlam("y", TMapp(TMvar("y"), TMvar("x"))))
val K = TMlam("x", TMlam("y", TMvar("x")))
val() = println!("K = ", K)
val J = subst(S, "x", z)
val() = println!("S = ", S)
val() = println!("J = ", J)
}
(* ****** ****** *)

(* end of [assign01-2_sol.dats] *)
