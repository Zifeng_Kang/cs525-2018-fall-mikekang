

local

val x = TMvar"x"
val y = TMvar"y"
val z = TMvar"z"

val omega = TMlam("x", x)
val Omega = TMapp(omega, omega)

val K = TMlam("x", TMlam("y", x))
val K' = TMlam("x", TMlam("y", y))

in

val () = assertloc(term_is_closed(K))
val () = assertloc(term_is_closed(K'))
val () = assertloc(term_is_closed(omega))
val () = assertloc(term_is_closed(Omega))

val () = assertloc(~term_is_closed(x))
val () = assertloc(~term_is_closed(TMapp(x, y)))

end (* end of [local] *)