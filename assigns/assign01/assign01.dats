(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 525
//
// Semester: Fall, 2018
//
// Classroom: FLR 112
// Class Time: TR 2:00-3:15
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date: Tuesday, the 25th of September
//
(* ****** ****** *)

typedef tvar = string

(* ****** ****** *)

datatype term =
  | TMint of int
(*
  | TMbool of bool
  | TMstring of string
*)
  | TMvar of tvar
  | TMlam of (tvar, term)
  | TMapp of (term, term)
  
(* ****** ****** *)
//
extern
fun
print_term : (term) -> void
and
prerr_term : (term) -> void
and
fprint_term : (FILEref, term) -> void
//
overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term
//
(* ****** ****** *)

implement
print_term(t0) =
fprint_term(stdout_ref, t0)
implement
prerr_term(t0) =
fprint_term(stderr_ref, t0)
implement
fprint_term(out, t0) =
(
case+ t0 of
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t1) =>
  fprint!(out, "TMlam(", x, "; ", t1, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
)

(* ****** ****** *)
//
// HX: 10 points
// Please count the number
// of *free* occurrences of x
// in a given term t0:
//
extern
fun
tvar_count
(t0: term, x: string): int
//
// For instance, there is
// zero occurrences of x in K;
// there are two free occurrences
// of z in x(z)(y(z)).
//
(* ****** ****** *)
//
// HX: 20 points:
// Please implement a function
// that checks whether a given term
// is closed, that is, containing no
// free variables. For instance, both
// K and S are closed.
//
extern
fun
term_is_closed(t0: term): bool
//
(* ****** ****** *)

(* end of [assign01.dats] *)
