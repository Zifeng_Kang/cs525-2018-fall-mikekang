(* ****** ****** *)

extern
fun
interp(t0: term): term

(* ****** ****** *)

extern
fun
subst0
(t0: term, x: tvar, t: term): term

(* ****** ****** *)

implement
subst0(t0, x, t) =
(
case+ t0 of
//
| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
//
| TMvar y =>
  if x = y then t else t0
//
| TMlam(y, t1) =>
  if x = y
  then t0 else TMlam(y, subst0(t1, x, t))
  // end of [if]
//
| TMapp(t1, t2) =>
  TMapp(subst0(t1, x, t), subst0(t2, x, t))
//
| TMlet(y, t1, t2) =>
  TMlet(y, t1, t2) where
  {
    val t1 = subst0(t1, x, t)
    val t2 =
      if x = y then t2 else subst0(t2, x, t)
    // end of [val]
  }
//
| TMopr(opr, us) =>
  TMopr(opr, list0_map(us, lam(u) => subst0(u, x, t)))
//
| TMift(t1, t2, t3) =>
  TMift(subst0(t1, x, t), subst0(t2, x, t), subst0(t3, x, t))
| TMfix(f1, x2, t3) =>
  if
  (x = f1)
  then t0 else
    (if x = x2 then t0 else TMfix(f1, x2, subst0(t3, x, t)))
  // end of [if]
//
) (* end of [subst0] *)

(* ****** ****** *)

local

fun
aux_app
(t0: term): term = let
//
val-TMapp(t1, t2) = t0
//
val t1 = interp(t1)
val t2 = interp(t2) // call-by-value
//
in
  case- t1 of
  | TMlam(x, t_body) =>
    interp(subst0(t_body, x, t2))
end // end of [aux_app]

fun
aux_let
(t0: term): term = let
//
val-TMlet(x, t1, t2) = t0
//
in
  interp(subst0(t2, x, interp(t1)))
end // end of [aux_opr]

fun
aux_opr
(t0: term): term = let
//
val-TMopr(opr, ts) = t0
//
val ts =
list0_map<term><term>(ts, lam(t) => interp(t))
//
in
//
ifcase
| opr = "+" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMint(i1+i2)
  end
| opr = "-" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMint(i1-i2)
  end
| opr = "*" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMint(i1*i2)
  end
//
| opr = "<" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMbool(i1 < i2)
  end
| opr = "<=" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMbool(i1 <= i2)
  end
| opr = ">" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMbool(i1 > i2)
  end
| opr = ">=" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMbool(i1 >= i2)
  end
//
| _ (* else *) =>
    (prerrln!("lambda0: interp: aux_opr: opr = ", opr); exit(1))
//
end // end of [aux_opr]

(*
static
fun aux_ift : term -> term
*)
fun
aux_ift
(t0: term): term = let
//
val-
TMift(t1, t2, t3) = t0
//
val t1 = interp(t1)
//
in
  case- t1 of
  | TMbool(tt) =>
    if tt then interp(t2) else interp(t3)
end // end of [aux_ift]

(*
static
fun aux_fix : term -> term
*)
fun
aux_fix
(t0: term): term = let
//
val-TMfix(f, x, t) = t0
//
in
  TMlam(x, subst0(t, f, t0))
end // end of [aux_fix]

in (* in-of-local *)

implement
interp(t0) =
(
case+ t0 of
| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
| TMvar _ => t0
| TMlam _ => t0
| TMapp _ => aux_app(t0)
| TMlet _ => aux_let(t0)
| TMopr _ => aux_opr(t0)
| TMift _ => aux_ift(t0)
| TMfix _ => aux_fix(t0)
)

end // end of [local]

(* ****** ****** *)

(* end of [interp.dats] *)
