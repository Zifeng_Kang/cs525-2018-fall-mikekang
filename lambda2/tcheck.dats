(* ****** ****** *)

(*
datatype type =
  | TPbas of string
  | TPfun of (type, type)
  | TPtup of (type, type)
*)

(* ****** ****** *)

val theSig =
g0ofg1
(
$list{$tup(topr, ctype)}
(
$tup("+", CTP(TPint :: TPint :: nil0(), TPint))
,
$tup("-", CTP(TPint :: TPint :: nil0(), TPint))
,
$tup("*", CTP(TPint :: TPint :: nil0(), TPint))
,
$tup("=", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup("!=", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup("<", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup(">", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup("<=", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup(">=", CTP(TPint :: TPint :: nil0(), TPbool))
)
)

typedef theSig_t = list0($tup(topr, ctype))

(* ****** ****** *)
//
extern
fun
tfind_opr : (topr) -> ctype
//
(* ****** ****** *)

implement
tfind_opr(opr) =
loop(theSig) where
{
fun
loop(kxs: theSig_t): ctype =
(
case- kxs of
| kx :: kxs => if kx.0 = opr then kx.1 else loop(kxs)
)
}

(* ****** ****** *)
//
extern
fun
eq_type_type
(T1: type, T2: type): bool
//
overload = with eq_type_type
//
(* ****** ****** *)
//
extern
fun
eq_typelst_typelst
(TS1: typelst, TS2: typelst): bool
//
overload = with eq_typelst_typelst
//
implement
eq_typelst_typelst
  (TS1, TS2) =
(
case+ (TS1, TS2) of
| (nil0(), nil0()) => true
| (T1 :: TS1, T2 :: TS2) => (T1 = T2) && (TS1 = TS2)
| (_, _) => false
)
//
(* ****** ****** *)

implement
eq_type_type
  (T1, T2) =
(
case+ T1 of
| TPbas(nm1) =>
  (
    case+ T2 of
    | TPbas(nm2) => nm1 = nm2 | _ => false
  )
| TPfun(T11, T12) =>
  (
    case+ T2 of
    | TPfun(T21, T22) =>
      (T11 = T21 && T12 = T22) | _ => false
  )
| TPtup(T11, T12) =>
  (
    case+ T2 of
    | TPtup(T21, T22) =>
      (T11 = T21 && T12 = T22) | _ => false
  )
)

(* ****** ****** *)

typedef
cntx = list0($tup(tvar, type))

(* ****** ****** *)

extern
fun
tcheck0 : term -> type
extern
fun
tcheck_cntx : (term, cntx) -> type

(* ****** ****** *)

implement
tcheck0(t0) =
tcheck_cntx(t0, list0_nil())

(* ****** ****** *)

implement
tcheck_cntx
  (t0, G0) = let
//
val () =
println! ("tcheck_cntx: t0 = ", t0)
//
in
(
case- t0 of
//
| TMint _ => TPint
| TMbool _ => TPbool
| TMstring _ => TPstring
//
| TMvar(x0) =>
  loop(G0) where
  {
    fun loop(xts: cntx): type =
    (
      case- xts of
      | xt :: xts =>
        if x0 = xt.0 then xt.1 else loop(xts)
    )
  }
//
| TMtup(t1, t2) =>
  TPtup(T1, T2) where
  {
    val T1 = tcheck_cntx(t1, G0)
    val T2 = tcheck_cntx(t2, G0)
  }
//
| TMlam(x1, T1, t2) =>
  TPfun(T1, T2) where
  {
    val G1 = $tup(x1, T1) :: G0
    val T2 = tcheck_cntx(t2, G1)
  }
//
| TMapp(t1, t2) => T12 where
  {
    val T1 = tcheck_cntx(t1, G0)
    val T2 = tcheck_cntx(t2, G0)
    val-TPfun(T11, T12) = T1
    val () = assertloc(T11 = T2)
  }
//
| TMfix
  (f1, x2, T1, T2, t3) => let
     val Tf = TPfun(T1, T2)
     val G1 = $tup(x2, T1) :: G0
     val G2 = $tup(f1, Tf) :: G1
   in
     tcheck_cntx(t3, G2)
   end
//
| TMift(t1, t2, t3) => T2 where
  {
    val T1 = tcheck_cntx(t1, G0)
    val () = assertloc(T1 = TPbool)
    val T2 = tcheck_cntx(t2, G0)
    val T3 = tcheck_cntx(t3, G0)
    val () = assertloc(T2 = T3)
  }
//
| TMopr(opr, ts) => T0 where
  {
    val CTP(TS1, T0) = tfind_opr(opr)
    val TS2 =
    list0_map<term><type>(ts, lam(t) => tcheck_cntx(t, G0))
    val () = assertloc(TS1 = TS2)
  }
//
)
end // end of [tcheck_cntx]

(* ****** ****** *)


(* ****** ****** *)

(* end of [tcheck.dats] *)
