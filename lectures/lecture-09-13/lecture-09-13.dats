(* ****** ****** *)

typedef tvar = string

(* ****** ****** *)

datatype term =
  | TMint of int
  | TMbool of bool
  | TMstring of string
  | TMvar of tvar
  | TMlam of (tvar, term)
  | TMapp of (term, term)
  
(* ****** ****** *)

val K =
TMlam("x", TMlam("y", TMvar("x")))

(* ****** ****** *)

val S =
TMlam
("x"
, TMlam("y"
       , TMlam("z", TMapp(TMapp(x, z), TMapp(y, z)))))
where
{
  val x = TMvar("x")
  val y = TMvar("y")
  val z = TMvar("z")
}

(* ****** ****** *)

val SKK = TMapp(TMapp(S, K), K)

(* ****** ****** *)

(* end of [lecture-09-13.dats] *)
