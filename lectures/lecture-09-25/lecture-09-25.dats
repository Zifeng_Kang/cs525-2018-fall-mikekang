(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#define :: list0_cons

(* ****** ****** *)

typedef tvar = string
typedef topr = string

(* ****** ****** *)

datatype term =
  | TMint of int // value
  | TMbool of bool // value
  | TMstring of string // value
  | TMvar of tvar // not evaluated
  | TMlam of (tvar, term) // value
  | TMapp of (term, term) // non-value
  | TMlet of (tvar, term, term) // let x = t1 in t2
  | TMopr of (topr, list0(term)) // primitive operators
//
  | TMift of (term, term, term)
  | TMfix of (tvar(*f*), tvar(*x*), term) // fixed-point opr
//  
(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(t0) = 
fprint_term(stdout_ref, t0)

local

implement
fprint_val<term> = fprint_term

in (* in-of-local *)

implement
fprint_term
  (out, t0) =
(
case+ t0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMbool(x) =>
  fprint!(out, "TMbool(", x, ")")
| TMstring(x) =>
  fprint!(out, "TMstring(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "; ", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMlet(x, t1, t2) =>
  fprint!(out, "TMlet(", x, "; ", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts)
//
| TMift(t1, t2, t3) =>
  fprint!(out, "TMift(", t1, "; ", t2, "; ", t3, ")")
| TMfix(f1, x2, t3) =>
  fprint!(out, "TMfix(", f1, "; ", x2, "; ", t3, ")")
//
)

end // end of [local]

(* ****** ****** *)

datatype value =
  | VALint of int
  | VALbool of bool
  | VALstring of string
  | VALclo of (term, envir)
  | VALfix of (term, envir)
  
where
envir = list0( $tup(tvar, value) )

(* ****** ****** *)

extern
fun
interp0(src: term): value
and
interp_env(src: term, env: envir): value

(* ****** ****** *)

implement
interp0(src) =
interp_env(src, list0_nil())

(* ****** ****** *)

extern
fun
aux_var:
(tvar, envir) -> value

implement
interp_env(t0, env) =
(
case- t0 of
//
| TMint(i) => VALint(i)
| TMbool(b) => VALbool(b)
//
| TMvar(x) => aux_var(x, env)
//
| TMlam(x, t) => VALclo(t0, env)
//
| TMapp(t1, t2) => let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
  in
    case- v1 of
    | VALclo
      (t_fun, env_fun) => let
        val-TMlam(x, t_body) = t_fun
      in
        interp_env
        (t_body, cons0( $tup(x, v2), env_fun ))
      end
    | VALfix(t_fix, env_fun) => let
        val-TMfix(f, x, t_body) = t_fix
        val env_fun =
          list0_cons( $tup(f, v1), env_fun )
        val env_fun =
          list0_cons( $tup(x, v2), env_fun )
      in
        interp_env(t_body, env_fun)
      end
  end
//
| TMfix(f, x, t_body) => VALfix(t0, env)
//
)

(* ****** ****** *)

(* end of [lecture-09-25.dats] *)
